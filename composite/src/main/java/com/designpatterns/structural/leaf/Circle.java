package com.designpatterns.structural.leaf;

import com.designpatterns.structural.basecomponent.Shape;

//leaf
public class Circle implements Shape {
    public void draw(String fillColor) {
        System.out.println("Draw a circle using color = " + fillColor);
    }


}
