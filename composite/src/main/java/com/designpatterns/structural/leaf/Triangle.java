package com.designpatterns.structural.leaf;

import com.designpatterns.structural.basecomponent.Shape;

//leaf
public class Triangle implements Shape {
    public void draw(String fillColor) {
        System.out.println("Drawing a triangle using color " + fillColor);
    }
}
