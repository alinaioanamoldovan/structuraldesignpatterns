package com.designpatterns.structural.composite;

import com.designpatterns.structural.basecomponent.Shape;

import java.util.ArrayList;
import java.util.List;

//Composite, Also implements base component and behaves similar to the leaf
// except that is contains groups of leafs
public class Drawing implements Shape {

    private List<Shape> shape = new ArrayList<Shape>();
    public void draw(String fillColor) {
        for (Shape s:shape){
            s.draw(fillColor);
        }
    }

    //  add a drawing
    public void addDrawing(Shape s){
        this.shape.add(s);
    }

    //remove a  drawing
    public void removeDrawing(Shape s){
        this.shape.remove(s);
    }

    //clear a drawing
    public void clearDrawings(){
        System.out.println("Removing all the drawings");
        this.shape.clear();
    }
}
