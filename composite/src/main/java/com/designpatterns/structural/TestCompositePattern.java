package com.designpatterns.structural;

import com.designpatterns.structural.basecomponent.Shape;
import com.designpatterns.structural.composite.Drawing;
import com.designpatterns.structural.leaf.Circle;
import com.designpatterns.structural.leaf.Triangle;

public class TestCompositePattern {

    public static void main(String[] args){
        Shape s1 = new Circle();
        Shape s2 = new Triangle();

        Drawing d = new Drawing();
        d.addDrawing(s1);
        d.addDrawing(s2);

        d.draw("Blue");



    }


}
