package com.designpatterns.structural.basecomponent;

//base component
public interface Shape {

    void draw(String fillColor);
}
