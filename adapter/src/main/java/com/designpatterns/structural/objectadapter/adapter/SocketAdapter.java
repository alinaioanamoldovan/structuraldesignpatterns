package com.designpatterns.structural.objectadapter.adapter;

import com.designpatterns.structural.objectadapter.model.Volt;

public interface SocketAdapter {

    Volt get3Volt();

    Volt get12Volt();

    Volt get120Volt();
}
