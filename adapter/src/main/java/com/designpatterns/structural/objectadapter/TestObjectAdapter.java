package com.designpatterns.structural.objectadapter;

import com.designpatterns.structural.objectadapter.adapter.SocketAdapter;
import com.designpatterns.structural.objectadapter.adapter.SocketAdapterImpl;
import com.designpatterns.structural.objectadapter.model.Volt;

/**
 * This implementation presents an implementation of the adapter design patter that uses
 * an object adapter. By object adapter means that the implementation of the adapter
 * inheritance will not be used, instead of inheritance composition will be used. And by composition
 * we use an instance of the source object
 */
public class TestObjectAdapter {

    public static void main(String[] args){

        SocketAdapter socketAdapter = new SocketAdapterImpl();
        Volt v3 = socketAdapter.get3Volt();
        Volt v12 =  socketAdapter.get12Volt();
        Volt v120 = socketAdapter.get120Volt();

        System.out.println("v3 is using object adapter " + v3.getVolts());
        System.out.println("v12 is using object adapter " + v12.getVolts());
        System.out.println("v120 is using object adapter " + v120.getVolts());
    }
}
