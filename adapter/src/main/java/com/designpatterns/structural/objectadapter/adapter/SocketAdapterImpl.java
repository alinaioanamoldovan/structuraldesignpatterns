package com.designpatterns.structural.objectadapter.adapter;

import com.designpatterns.structural.objectadapter.model.Socket;
import com.designpatterns.structural.objectadapter.model.Volt;

public class SocketAdapterImpl implements SocketAdapter {

    //uses composition in order to implement the adapter
    private Socket socket = new Socket();
    public Volt get3Volt() {
        Volt v = socket.getVolt();
        return convertVolt(v,40);
    }

    public Volt get12Volt() {
        Volt v = socket.getVolt();
        return convertVolt(v,10);
    }

    public Volt get120Volt() {
        return socket.getVolt();
    }

    public Volt convertVolt(Volt v, int i){
        return new Volt(v.getVolts()/i);
    }
}
