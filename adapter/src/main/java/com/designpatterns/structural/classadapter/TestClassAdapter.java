package com.designpatterns.structural.classadapter;

import com.designpatterns.structural.classadapter.adapter.SocketAdapter;
import com.designpatterns.structural.classadapter.adapter.SocketAdapterImpl;
import com.designpatterns.structural.classadapter.model.Volt;

/**
 * This implementation is using class adapter. Means that the implementation of the
 * adapter interface will extend an object. So the class adapter is uses
 * inheritance for the implementation
 */
public class TestClassAdapter {

    public static void main(String[] args) {

        SocketAdapter socketAdapter = new SocketAdapterImpl();

        Volt v3 = socketAdapter.get3Volt();
        Volt v12 = socketAdapter.get12Volt();
        Volt v120 = socketAdapter.get120Volt();
        System.out.println("v3 is using class adapter " + v3.getVolts());
        System.out.println("v12 is using class adapter " + v12.getVolts());
        System.out.println("v120 is using class adapter " + v120.getVolts());
    }
}
