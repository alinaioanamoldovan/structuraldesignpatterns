package com.designpatterns.structural.classadapter.model;

public class Socket {

    public Volt getVolt(){
        return new Volt(120);
    }
}
