package com.designpatterns.structural.classadapter.adapter;

import com.designpatterns.structural.classadapter.model.Volt;

public interface SocketAdapter {

    Volt get120Volt();

    Volt get3Volt();

    Volt get12Volt();
}
