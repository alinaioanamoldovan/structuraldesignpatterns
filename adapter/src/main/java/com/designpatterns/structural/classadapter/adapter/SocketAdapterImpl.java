package com.designpatterns.structural.classadapter.adapter;

import com.designpatterns.structural.classadapter.model.Socket;
import com.designpatterns.structural.classadapter.model.Volt;

public class SocketAdapterImpl extends Socket implements SocketAdapter {
    public Volt get120Volt() {
        return getVolt();
    }

    public Volt get3Volt() {
        Volt v = getVolt();
        return convertVolt(v,40);
    }

    public Volt get12Volt() {
        Volt v = getVolt();
        return convertVolt(v,10);
    }

    public Volt convertVolt(Volt v,int i){
        return new Volt(v.getVolts()/i);
    }
}
